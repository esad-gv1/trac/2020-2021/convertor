'use strict';

// file system : importe un module de fichiers et path de chemin
const fs = require('fs');
const path = require('path');

const baseDir = "./assets";

const dirContent = fs.readdirSync(baseDir, { "withFileTypes": true });
// console.log(dirContent);

let outputString = "";

for (let i = 0; i < dirContent.length; i++) {

    const entry = dirContent[i];

    if (entry.isDirectory()) {

        const subDir = path.join(baseDir, entry.name);
        const subDirContent = fs.readdirSync(subDir);
        //console.log(subDirContent);

        outputString += "var " + entry.name + " = [\n";

        for (let j = 0; j < subDirContent.length; j++) {

            let tempFileName = subDirContent[j];

            if (tempFileName.indexOf(".png") >= 0) {
                let fileName = tempFileName.substring(0, tempFileName.length - 4);
                outputString += "'" + fileName + "',\n";
            }
        }
        outputString += "];" + "\n";
    }
    outputString += "\n";
}

console.log(outputString);

fs.writeFileSync(path.join("./js", "assetsArray.js"), outputString);