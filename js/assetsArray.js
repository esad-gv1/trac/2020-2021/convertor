
var Animations = [
'01_vid',
'03_vid',
'04_vid',
'05_vid',
'06_vid',
'07_vid',
'08_vid',
];

var Basiques = [
'500x500_CercleVide2_r10g10b10',
'500x500_CercleVide_r10g10b10',
'500x500_Cercle_r10g10b10',
'500x500_Rect10px_r10g10b10',
'500x500_Rect_r10g10b10',
'500x500_Trait1_r10b10g10',
'500x500_Trait2_r10g10b10',
'500x500_TraitNet1_r10b10g10',
'500x500_TraitNet1_r255b10g10',
'500x500_TraitNet2_r10g10b10',
'500x500_TriangleNet_r10b10g10',
'500x500_Triangle_r10b10g10',
'500x500_croix_r10g10b10',
];

var Court = [
'Clarinette_C',
'Clarinette_D',
'Clarinette_E_F',
'Violon_AB',
'Violon_D',
'Violon_K',
'Violon_Q',
];

var GeoTortue = [
'GeoTortue003',
'GeoTortue004',
'GeoTortue009',
];

var Grille = [
'2T1',
'2T2',
'2T3',
'2T4',
'2T5',
'2T6',
'Grille1',
'Grille1_1',
'Grille2',
'Grille3',
'Grille4',
'Grille5',
'Grille6',
'Grille7',
'Grille8',
'Grille9',
'H1_Perc',
'H1_basse',
'H2_Perc',
'H2_basse',
'H2_notes',
'H4_notes',
'T1',
'T2',
'T2_T3',
'T3',
'T4',
'T4_T5',
'T5',
'U1',
'U2',
'U3',
'U4',
];

var Grille_2 = [
'Compoisition_01',
'Compoisition_03',
'Composition_02',
'Composition_05',
'Composition_07',
'Composition_08',
'Drum_01',
'FondSonore',
'Melodie_01',
'Melodie_02',
'Melodie_03',
'Melodie_05',
'Melodie_06',
'Melodie_07',
'Melodie_08',
'Melodie_09',
'Melodie_10',
'Melodie_11',
'Melodie_12',
'Melodie_13',
'Melodie_14',
'Melodie_15',
'Melodie_16',
'Melodie_17',
'Melodie_18',
'Melodie_19',
'Melodie_20',
'Melodie_21',
'Mix_01',
];

var Libres = [
'test_img12',
'test_img_13',
'test_img_14',
'test_img_15',
'violon_M_reotur',
'violon_O_retour',
'woof_woof_2',
];


var Traits = [
'T30L100S70',
'T90L100S70+Sombre',
'Traits1',
'Traits2',
'Traits3',
'Traits4',
'Traits6',
'Traits_FondNoir',
];

