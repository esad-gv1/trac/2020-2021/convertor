window.addEventListener("load", Setup);
var Xpos;
var sources = [];
var Compteur = 1;
var CompteurMenu = 0;
var CompteurWarning = 0;

function Setup(){

    var ContainerTitre = document.getElementById("ContainerTitre");
    ContainerTitre.style.marginRight = "auto";
    //HOME
    var Home = document.getElementById("Home");
    var HomeCache = document.getElementById("HomeCache");
    Home.addEventListener("mouseover", TracIn);
    Home.addEventListener("mouseout", TracOut);
    Home.addEventListener("click", GoPageHome);
    
    function TracIn(){
        HomeCache.style.fontSize = 15+"px";
    }
    function TracOut(){
        HomeCache.style.fontSize = 0+"px";
    }
    function GoPageHome(){
        window.location = "index.html";
    }

    //PRESENTATION
    var Quesako = document.getElementById("Quesako");
    var Presentation = document.getElementById("Presentation");
    Quesako.addEventListener("mouseover", PresentationIn);
    Quesako.addEventListener("mouseout", PresentationOut);

    function PresentationIn(){
        Presentation.style.fontSize=15+"px";
        Presentation.style.backgroundColor="#0b0b0b";
    }
    function PresentationOut(){
        Presentation.style.fontSize=0+"px";
        Presentation.style.backgroundColor="transparent";
    }


   

    
}