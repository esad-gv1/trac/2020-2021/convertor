window.addEventListener("load", Setup);
var activeDiv = null;


function Setup() {

    var AudioContext = window.AudioContext // Default
    || window.webkitAudioContext // Safari and old versions of Chrome
    || false; 

if (AudioContext) {
    // Do whatever you want using the Web Audio API
       // DEUXIEME EXPERIENCE 

       var isDrawing = false;
       var x = 0;
       var y = 0;
   
       let canvas = document.getElementById("audio_visual");
       let ctx = canvas.getContext("2d");
   
   
       var Canvas = document.getElementById("MusicDessin");
       Canvas.width = 150;
       Canvas.height = 150;
       Canvas.style.backgroundColor = "grey";
       var MusicDessin = Canvas.getContext("2d");
   
       var rect = Canvas.getBoundingClientRect();
   
       //Prévenir que le canvas pour intéragir est celui de gauche
       canvas.addEventListener("mouseenter", GoController);
       canvas.addEventListener("mouseout", RemoveControl);
   
       function GoController() {
           canvas.style.backgroundColor = "grey";
           ctx.font = "15px serif";
           ctx.fillText("Ceci est l'écran de visualisation.", 10, 20);
           ctx.fillText("Dessinez via le contrôleur --->", 10, 50);
       }
       function RemoveControl() {
           canvas.style.backgroundColor = "white";
           ctx.clearRect(0, 0, 500, 500);
       }
   
       Canvas.addEventListener("mousedown", DemarrerDessin);
   
       function DemarrerDessin(e) {
           canvas.removeEventListener("mouseenter", GoController);
           canvas.removeEventListener("mouseout", RemoveControl);
           x = e.clientX - rect.left;
           y = e.clientY - rect.top;
           isDrawing = true;
       }
   
       Canvas.addEventListener("mousemove", DessinEnCours);
       function DessinEnCours(e) {
           if (isDrawing === true) {
               DrawNsound(MusicDessin, x, y, e.clientX - rect.left, e.clientY - rect.top);
               x = e.clientX - rect.left;
               y = e.clientY - rect.top;
           }
       }
       window.addEventListener("mouseup", StopAuDessin);
       function StopAuDessin(e) {
           if (isDrawing === true) {
               DrawNsound(MusicDessin, x, y, e.clientX - rect.left, e.clientY - rect.top);
               x = 0;
               y = 0;
               isDrawing = false;
           }
       }
   
   
   
       function DrawNsound(MusicDessin) {
           
           MusicDessin.beginPath();
           MusicDessin.strokeStyle = "black";
           MusicDessin.lineWidth = 3;
           MusicDessin.rect(x, y, 5, 15);
           MusicDessin.stroke();
           MusicDessin.closePath();
           ctx.globalAlpha = 0.8;
   
           function GetRandomLocationX() {
               const randomX = Math.floor(Math.random() * (300 - 0 + 1)) + 0;
               return PointDepartX = randomX;
   
           }
   
           var PALETTE = [
               'fuschia',
               'chartreuse',
               'limegreen',
               'pink',
               'olive',
               'silver',
               'aqua',
               'coral',
               'crimson'
   
           ]
           //RANDOM IN ARRAY
           function getRandomFromPalette() {
               const rando2 = Math.floor(Math.random() * (PALETTE.length - 0 + 1)) + 0;
               return strokeColor = PALETTE[rando2];
           }
           if (x > 125) {
               var context = new AudioContext();
               var oscillator = context.createOscillator();
               var g = context.createGain();
               draw();
               function draw() {
                   ctx.beginPath();
                   ctx.fillRect(x, 40, 15, y);
                   ctx.fillStyle = getRandomFromPalette();
                   ctx.closePath();
   
               }
   
               oscillator.type = "sawtooth";
               var frequency = y * x;
               oscillator.frequency.value = frequency;
               oscillator.connect(g);
               g.connect(context.destination);
               oscillator.start(y); g.gain.exponentialRampToValueAtTime(0.00001, context.currentTime + 1);
           }
   
           else if (x > 75) {
               var context = new AudioContext();
               var oscillator = context.createOscillator();
               var g = context.createGain();
               draw();
               function draw() {
                   ctx.beginPath();
                   ctx.fillRect(x, GetRandomLocationX(), 30, canvas.height);
                   ctx.fillStyle = getRandomFromPalette();
                   ctx.closePath();
               }
   
               oscillator.type = "sawtooth";
               var frequency = y;
               oscillator.frequency.value = frequency;
               oscillator.connect(g);
               g.connect(context.destination);
               oscillator.start(0);
               g.gain.exponentialRampToValueAtTime(0.00001, context.currentTime + 1);
           }
           else if (x > 57) {
               var context = new AudioContext();
               var oscillator = context.createOscillator();
               var g = context.createGain();
               draw();
               function draw() {
                   ctx.beginPath();
                   ctx.fillRect(x, 260, canvas.width, 10);
                   ctx.fillStyle = "fuschia";
                   ctx.closePath();
               }
               var frequency = x - 10;
               oscillator.type = "sawtooth";
               oscillator.frequency.value = frequency;
               oscillator.connect(g);
               oscillator.start(0);
               g.connect(context.destination);
               g.gain.exponentialRampToValueAtTime(0.00001, context.currentTime + 1);
           }
           else if (x < 42) {
               var context = new AudioContext();
               var oscillator = context.createOscillator();
               var g = context.createGain();
               draw();
               function draw() {
                   ctx.beginPath();
                   ctx.fillRect(10, GetRandomLocationX(), 30, 30);
                   ctx.fillStyle = "red";
                   ctx.closePath();
   
               }
               var frequency = x - 10;
               oscillator.type = "sawtooth";
               oscillator.frequency.value = frequency;
               oscillator.connect(g);
               oscillator.start(0);
               g.connect(context.destination);
               g.gain.exponentialRampToValueAtTime(0.00001, context.currentTime + 1);
           }
   
           else if (x < 10) {
               var context = new AudioContext();
               var oscillator = context.createOscillator();
               var g = context.createGain();
               draw();
               function draw() {
                   ctx.beginPath();
                   ctx.fillRect(80, GetRandomLocationX(), 30, canvas.width);
                   ctx.fillStyle = "white";
                   ctx.closePath();
               }
               oscillator.type = "square";
               var frequency = y;
               oscillator.frequency.value = frequency;
               oscillator.connect(g);
               g.connect(context.destination);
               oscillator.start(0);
               g.gain.exponentialRampToValueAtTime(0.00001, context.currentTime + 1);
           }
           else if (x < 10) {
               var context = new AudioContext();
               var oscillator = context.createOscillator();
               var g = context.createGain();
               draw();
               function draw() {
                   ctx.beginPath();
                   ctx.fillStyle = "purple";
                   ctx.fillRect(GetRandomLocationX(), GetRandomLocationX(), GetRandomLocationX(), GetRandomLocationX());
                   ctx.closePath();
               }
               oscillator.type = "triangle";
               var frequency = y;
               oscillator.frequency.value = frequency;
               oscillator.connect(g);
               g.connect(context.destination);
               oscillator.start(0);
               g.gain.exponentialRampToValueAtTime(0.00001, context.currentTime + 1);
           }
           if (y > 70) {
               var context = new AudioContext();
               var oscillator = context.createOscillator();
               var g = context.createGain();
               let analyser = context.createAnalyser();
               analyser.fftSize = 2048;
               g.connect(analyser);
               let data = new Uint8Array(analyser.frequencyBinCount);
               requestAnimationFrame(loopingFunction);
   
               function loopingFunction() {
                   requestAnimationFrame(loopingFunction);
                   analyser.getByteFrequencyData(data);
                   draw(data);
               }
               function draw(data) {
                   data = [...data];
   
                   let space = canvas.height / data.length;
                   data.forEach((value, i) => {
                       ctx.beginPath();
                       ctx.moveTo(space, canvas.width); //x,y
                       ctx.lineTo(space, canvas.width - value); //x,y
                       ctx.strokeStyle = "pink";
                       ctx.stroke();
                   })
               }
               var frequency = x;
               oscillator.type = "sawtooth";
               oscillator.frequency.value = frequency;
               oscillator.connect(g);
               oscillator.start(0);
               g.connect(context.destination);
               g.gain.exponentialRampToValueAtTime(0.00001, context.currentTime + 1);
           }
   
   
   
   
           else if (y < 50) {
               var context = new AudioContext();
               var oscillator = context.createOscillator();
               var g = context.createGain();
               draw();
               function draw() {
                   ctx.beginPath();
                   ctx.fillRect(x, GetRandomLocationX(), GetRandomLocationX(), GetRandomLocationX());
                   ctx.fillStyle = "fuschia";
                   ctx.closePath();
               }
               oscillator.type = "sawtooth";
               var frequency = y - x;
               oscillator.frequency.value = frequency;
               var g = context.createGain();
               oscillator.connect(g);
               g.connect(context.destination);
               oscillator.start(0); g.gain.exponentialRampToValueAtTime(0.00001, context.currentTime + 1);
           }
       }
} else {
    // Web Audio API is not supported
    // Alert the user
    alert("Sorry, but the Web Audio API is not supported by your browser. Please, consider upgrading to the latest version or downloading Google Chrome or Mozilla Firefox");
}
    

 

}