window.addEventListener("load", Setup);
var Xpos;
var sources = [];
var Compteur = 1;
var CompteurMenu = 0;
var CompteurWarning = 0;
var CompteurVideo = 0;

function Setup() {

    //Accueil
    var Home = document.getElementById("Home");
    var HomeCache = document.getElementById("HomeCache");
    Home.addEventListener("mouseover", TracIn);
    Home.addEventListener("mouseout", TracOut);
    Home.addEventListener("click", GoPageHome);

    function TracIn() {
        HomeCache.style.fontSize = 15 + "px";
    }

    function TracOut() {
        HomeCache.style.fontSize = 0 + "px";
    }

    function GoPageHome() {
        window.location = "index.html";
    }

    //PRESENTATION
    var Quesako = document.getElementById("Quesako");
    var Presentation = document.getElementById("Presentation");
    Quesako.addEventListener("mouseover", PresentationIn);
    Quesako.addEventListener("mouseout", PresentationOut);

    function PresentationIn() {
        Presentation.style.fontSize = 15 + "px";
        Presentation.style.backgroundColor = "white";
    }

    function PresentationOut() {
        Presentation.style.fontSize = 0 + "px";
        Presentation.style.backgroundColor = "transparent";
    }

    /// GALERIE D'IMAGES

    // Le grand container qui contient toutes les catégories
    var ContainerAllMosaique = document.getElementById("ContainerAllMosaique");
    //Les catégories
    var NomsCategories = ["Basiques", "Grille", "Libres", "Court", "Animations", "Traits", "Grille_2"];
    var Categories = [Basiques, Grille, Libres, Court, Animations, Traits, Grille_2];



    // Création des containers qui contiendront les images
    for (i = 0; i < Categories.length; i++) {
        var TexteContainer = document.createElement("p");
        var ContainerMosaique = document.createElement("div");
        ContainerMosaique.classList.add('ContainerMosaique');
        TexteContainer.classList.add('TexteContainer');
        TexteContainer.innerHTML = NomsCategories[i];
        ContainerAllMosaique.appendChild(TexteContainer);
        ContainerAllMosaique.appendChild(ContainerMosaique);
    }
    //Récupération des containers
    var ContainerMosaique = document.getElementsByClassName("ContainerMosaique");

    //Création des images

    //Container d'images 0 Basiques
    for (i = 0; i < Categories[0].length; i++) {
        CompteurVideo = 0;
        var img = document.createElement("img");
        img.classList.add('Mosaique0');
        img.classList.add('ToutesImagesMosaiques');
        ContainerMosaique[0].appendChild(img);
    }
    var Mosaique0 = document.getElementsByClassName("Mosaique0");
    for (i = 0; i < Mosaique0.length; i++) {
        Mosaique0[i].addEventListener("click", ChangeSource0);
        Mosaique0[i].customIndex = i;
        Mosaique0[i].src = "assets/" + NomsCategories[0] + "/" + Basiques[i] + ".png";
    }

    function ChangeSource0(event) {
        NewSource0 = event.target;
        //A changer a chaque fois
        Image.src = "assets/" + NomsCategories[0] + "/" + Basiques[NewSource0.customIndex] + ".png";
        Audio.src = "assets/" + NomsCategories[0] + "/" + Basiques[NewSource0.customIndex] + ".wav";
    }

    //Container d'images 1 Grille
    for (i = 0; i < Categories[1].length; i++) {
        CompteurVideo = 0;
        var img = document.createElement("img");
        img.classList.add('Mosaique1');
        img.classList.add('ToutesImagesMosaiques');
        ContainerMosaique[1].appendChild(img);
    }
    var Mosaique1 = document.getElementsByClassName("Mosaique1");
    //Au clic sur les div, changement de source
    for (i = 0; i < Mosaique1.length; i++) {
        Mosaique1[i].addEventListener("click", ChangeSource1);
        Mosaique1[i].customIndex = i;
        Mosaique1[i].src = "assets/" + NomsCategories[1] + "/" + Grille[i] + ".png";
    }

    function ChangeSource1(event) {
        NewSource1 = event.target;
        Image.src = "assets/" + NomsCategories[1] + "/" + Grille[NewSource1.customIndex] + ".png";
        Audio.src = "assets/" + NomsCategories[1] + "/" + Grille[NewSource1.customIndex] + ".wav";
    }
    //Container d'images 2 Libres
    for (i = 0; i < Categories[2].length; i++) {
        var img = document.createElement("img");
        img.classList.add('Mosaique2');
        img.classList.add('ToutesImagesMosaiques');
        ContainerMosaique[2].appendChild(img);
    }
    var Mosaique2 = document.getElementsByClassName("Mosaique2");
    for (i = 0; i < Mosaique2.length; i++) {
        Mosaique2[i].addEventListener("click", ChangeSource2);
        Mosaique2[i].customIndex = i;
        //A changer a chaque fois
        Mosaique2[i].src = "assets/" + NomsCategories[2] + "/" + Libres[i] + ".png";
    }

    function ChangeSource2(event) {
        CompteurVideo = 0;
        NewSource2 = event.target;
        //A changer a chaque fois
        Image.src = "assets/" + NomsCategories[2] + "/" + Libres[NewSource2.customIndex] + ".png";
        Audio.src = "assets/" + NomsCategories[2] + "/" + Libres[NewSource2.customIndex] + ".wav";
    }
    //Container d'images 3 Court
    for (i = 0; i < Categories[3].length; i++) {
        CompteurVideo = 0;
        var img = document.createElement("img");
        img.classList.add('Mosaique3');
        img.classList.add('ToutesImagesMosaiques');
        ContainerMosaique[3].appendChild(img);
    }
    var Mosaique3 = document.getElementsByClassName("Mosaique3");
    for (i = 0; i < Mosaique3.length; i++) {
        Mosaique3[i].addEventListener("click", ChangeSource3);
        Mosaique3[i].customIndex = i;
        //A changer a chaque fois
        Mosaique3[i].src = "assets/" + NomsCategories[3] + "/" + Court[i] + ".png";
    }

    function ChangeSource3(event) {
        NewSource3 = event.target;
        //A changer a chaque fois
        Image.src = "assets/" + NomsCategories[3] + "/" + Court[NewSource3.customIndex] + ".png";
        Audio.src = "assets/" + NomsCategories[3] + "/" + Court[NewSource3.customIndex] + ".wav";
    }
    //Container d'images 4 ANIMATIONS /!!!!!

    for (i = 0; i < Categories[4].length; i++) {
        var img = document.createElement("img");
        img.classList.add('Mosaique4');
        img.classList.add('ToutesImagesMosaiques');
        ContainerMosaique[4].appendChild(img);
    }
    var Mosaique4 = document.getElementsByClassName("Mosaique4");
    for (i = 0; i < Mosaique4.length; i++) {
        Mosaique4[i].addEventListener("click", ChangeSource4);

        Mosaique4[i].customIndex = i;
        //A changer a chaque fois
        Mosaique4[i].src = "vid/" + Animations[i] + ".png";
    }

    function ChangeSource4(event) {
        CompteurVideo++;
        let video = document.querySelector(".video");
        video.style.display = "block";
        NewSource4 = event.target;
        //A changer a chaque fois
        video.src = "vid/" + Animations[NewSource4.customIndex] + ".mov";
        Audio.src = "";
        Image.src = "";
    }

    //Container d'images 5
    for (i = 0; i < Categories[5].length; i++) {
        var img = document.createElement("img");
        img.classList.add('Mosaique5');
        img.classList.add('ToutesImagesMosaiques');
        ContainerMosaique[5].appendChild(img);
    }
    var Mosaique5 = document.getElementsByClassName("Mosaique5");
    for (i = 0; i < Mosaique5.length; i++) {
        Mosaique5[i].addEventListener("click", ChangeSource5);
        Mosaique5[i].customIndex = i;
        //A changer a chaque fois
        Mosaique5[i].src = "assets/" + NomsCategories[5] + "/" + Traits[i] + ".png";
    }

    function ChangeSource5(event) {
        NewSource5 = event.target;
        //A changer a chaque fois
        Image.src = "assets/" + NomsCategories[5] + "/" + Traits[NewSource5.customIndex] + ".png";
        Audio.src = "assets/" + NomsCategories[5] + "/" + Traits[NewSource5.customIndex] + ".wav";
    }

    //Container d'images 6
    for (i = 0; i < Categories[6].length; i++) {
        var img = document.createElement("img");
        img.classList.add('Mosaique6');
        img.classList.add('ToutesImagesMosaiques');
        ContainerMosaique[6].appendChild(img);
    }
    var Mosaique6 = document.getElementsByClassName("Mosaique6");
    for (i = 0; i < Mosaique6.length; i++) {
        Mosaique6[i].addEventListener("click", ChangeSource6);
        Mosaique6[i].customIndex = i;
        //A changer a chaque fois
        Mosaique6[i].src = "assets/" + NomsCategories[6] + "/" + Grille_2[i] + ".png";
    }

    function ChangeSource6(event) {
        NewSource6 = event.target;
        //A changer a chaque fois
        Image.src = "assets/" + NomsCategories[6] + "/" + Grille_2[NewSource6.customIndex] + ".png";
        Audio.src = "assets/" + NomsCategories[6] + "/" + Grille_2[NewSource6.customIndex] + ".wav";
    }
    //PLAYER AUDIO // VIDEO -- LIE A L'IMAGE
    let audio = document.querySelector(".audio");
    let video = document.querySelector(".video");

    let BarreMouvante = document.querySelector(".BarreMouvante");
    let btn = document.getElementById("Play-Pause");
    let muteBtn = document.getElementById("Mute");
    let VolumeSlide = document.getElementById("volumeSlide");
    let BarreLecture = document.querySelector(".BarreLecture");
    var Image = document.getElementById("Image");

    var DownloadSon = document.getElementById("DownloadSon");
    DownloadSon.addEventListener("click", DownloadSoundFiles);
    var DownloadImage = document.getElementById("DownloadImage");
    DownloadImage.addEventListener("click", DownloadImgFiles);

    function DownloadSoundFiles() {
        var SourceSon = Audio.getAttribute('src');
        DownloadSon.href = SourceSon;
    }

    function DownloadImgFiles() {
        var SourceImg = Image.getAttribute('src');
        DownloadImage.href = SourceImg;
    }


    function togglePlayPause() {
        if (CompteurVideo == 0) {
            let video = document.querySelector(".video");
            video.style.display = "none";
            // Audio
            if (audio.paused) {
                btn.className = "pause";
                audio.play();
            } else {
                btn.className = "play";
                audio.pause();
            }
        }
        // Video
        if (CompteurVideo >= 1) {
            if (video.paused) {
                btn.className = "pause";
                video.play();
            } else {
                btn.className = "play";
                video.pause();
            }

            video.addEventListener("timeupdate", function() {
                let juicePose = video.currentTime / video.duration;
                BarreMouvante.style.width = juicePose * 100 + "%";
                if (video.ended) {
                    btn.className = "play";
                }
            })

        }

    }
    btn.onclick = function() {
        togglePlayPause();
    }
    Image.onclick = function() {
        togglePlayPause();
    }
    video.onclick = function() {
            togglePlayPause();
        }
        //Barre mouvante

    audio.addEventListener("timeupdate", function() {
        let juicePose = audio.currentTime / audio.duration;
        BarreMouvante.style.width = juicePose * 100 + "%";
        if (audio.ended) {
            btn.className = "play";
        }
    })

    //Mute l'audio
    muteBtn.addEventListener("click", function() {
        if (audio.muted) {
            audio.muted = false;
            muteBtn.innerHTML = "Mute";
        } else {
            audio.muted = true;
            muteBtn.innerHTML = "Unmute";
        }
    })

    //Volume

    VolumeSlide.addEventListener('change', function() {
        audio.volume = VolumeSlide.value / 100;
    })

    //Barre Mouvante qui change le time

    let rect = BarreLecture.getBoundingClientRect();
    console.log(rect);
    let largeur = rect.width;
    BarreLecture.addEventListener("click", function(e) {
        //La valeur de notre click sur x par rapport a notre élément
        let x = e.clientX - rect.left;
        //Convertir en Pourcentage
        let widthPercent = ((x * 100) / largeur);

        let currentTimeTrue = (widthPercent * audio.duration) / 100;

        audio.currentTime = currentTimeTrue;

        BarreMouvante.style.width = widthPercent + "%";
    })

    //Tete de lecture 
    var Tete_Lecture = document.getElementById("Tete_Lecture");
    var Audio = document.querySelector(".audio");
    Audio.addEventListener("timeupdate", TeteDepart);

    var Image = document.getElementById("Image");
    TailleImage = Image.width;

    function TeteDepart() {
        //                    Xpos = Audio.currentTime / Audio.duration;
        Xpos = Audio.currentTime / Audio.duration;
        Tete_Lecture.style.left = Xpos * 100 + "%";

    }
    var ContainerSon_Image = document.getElementById("ContainerSon_Image");
    var Warning = document.getElementById("Warning");

    AfficherWarning();

    function AfficherWarning() {
        Warning.style.display = "block";

    }
    Warning.addEventListener("click", WarningOk);

    function WarningOk() {
        Warning.style.display = "none";
    }


}